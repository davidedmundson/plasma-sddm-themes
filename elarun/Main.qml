/***************************************************************************
* Copyright (c) 2013 Reza Fatahilah Shah <rshah0385@kireihana.com>
* Copyright (c) 2013 Abdurrahman AVCI <abdurrahmanavci@gmail.com>
* Copyright (c) 2013 Martin Klapetek <mklapetek@kde.org>
* Copyright (c) 2013 David Edmundson <davidedmudson@kde.org>
* 
*
* Permission is hereby granted, free of charge, to any person
* obtaining a copy of this software and associated documentation
* files (the "Software"), to deal in the Software without restriction,
* including without limitation the rights to use, copy, modify, merge,
* publish, distribute, sublicense, and/or sell copies of the Software,
* and to permit persons to whom the Software is furnished to do so,
* subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included
* in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
* OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
* OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
* OR OTHER DEALINGS IN THE SOFTWARE.
*
***************************************************************************/

import QtQuick 2.0
import SddmComponents 2.0
import org.kde.plasma.components 2.0 as PlasmaComponents
import org.kde.plasma.core 2.0 as PlasmaCore

Rectangle {
    width: 640
    height: 480

    TextConstants { id: textConstants }

    Connections {
        target: sddm
        onLoginSucceeded: {
        }
        onLoginFailed: {
        }
    }

    Repeater {
        model: screenModel
        Background {
            x: geometry.x; y: geometry.y; width: geometry.width; height:geometry.height
            source: config.background
            fillMode: Image.PreserveAspectCrop
        }
    }

    Rectangle {
        property variant geometry: screenModel.geometry(screenModel.primary)
        x: geometry.x; y: geometry.y; width: geometry.width; height: geometry.height
        color: "transparent"

        Rectangle {
            width: 416
            height: 262
            color: "#00000000"

            anchors.centerIn: parent

            Image {
                anchors.fill: parent
                source: "images/rectangle.png"
            }

            Image {
                anchors.fill: parent
                source: "images/rectangle_overlay.png"
                opacity: 0.1
            }

            Item {
                anchors.margins: 20
                anchors.fill: parent

                Text {
                    height: 50
                    anchors.top: parent.top
                    anchors.left: parent.left; anchors.right: parent.right

                    color: "#0b678c"
                    opacity: 0.75

                    text: sddm.hostName

                    font.bold: true
                    font.pixelSize: 18
                }

                Column {
                    anchors.centerIn: parent

                    Row {
                        PlasmaCore.IconItem { source: "user-identity" }

                        PlasmaComponents.TextField {
                            id: user_entry
                            anchors.verticalCenter: parent.verticalCenter;
                            text: userModel.lastUser
                            KeyNavigation.backtab: layoutBox; KeyNavigation.tab: pw_entry
                        }
                    }

                    Row {

                        PlasmaCore.IconItem { source: "document-encrypt" }

                        PlasmaComponents.TextField {
                            id: pw_entry
                            anchors.verticalCenter: parent.verticalCenter;
                            echoMode: TextInput.Password

                            KeyNavigation.backtab: user_entry; KeyNavigation.tab: login_button

                            Keys.onPressed: {
                                if (event.key === Qt.Key_Return || event.key === Qt.Key_Enter) {
                                    sddm.login(user_entry.text, pw_entry.text, menu_session.index)
                                    event.accepted = true
                                }
                            }
                        }
                    }
                }

                ImageButton {
                    id: login_button
                    anchors.right: parent.right
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.margins: 20

                    source: "images/login_normal.png"

                    onClicked: sddm.login(user_entry.text, pw_entry.text, menu_session.index)

                    KeyNavigation.backtab: pw_entry; KeyNavigation.tab: session_button
                }

                Item {
                    height: 20
                    anchors.bottom: parent.bottom
                    anchors.left: parent.left; anchors.right: parent.right

                    Row {
                        id: buttonRow
                        anchors.left: parent.left
                        anchors.verticalCenter: parent.verticalCenter

                        spacing: 8

                        IconButton {
                            id: session_button
                            source: "system-log-out"
                            onClicked: sessionMenu.open()

                            KeyNavigation.backtab: login_button; KeyNavigation.tab: system_button
                        }

                        IconButton {
                            id: system_button
                            source: "system-shutdown"
                            onClicked: sddm.powerOff()

                            KeyNavigation.backtab: session_button; KeyNavigation.tab: reboot_button
                        }

                        IconButton  {
                            id: reboot_button
                            source: "system-reboot"
                            onClicked: sddm.reboot()

                            KeyNavigation.backtab: system_button; KeyNavigation.tab: suspend_button
                        }

                        IconButton  {
                            id: suspend_button
                            source: "system-suspend"
                            visible: sddm.canSuspend
                            onClicked: sddm.suspend()

                            KeyNavigation.backtab: reboot_button; KeyNavigation.tab: hibernate_button
                        }

                        IconButton  {
                            id: hibernate_button
                            source: "system-hibernate"
                            visible: sddm.canHibernate
                            onClicked: sddm.hibernate()

                            KeyNavigation.backtab: suspend_button; KeyNavigation.tab: session
                        }
                    }

                    Text {
                        id: time_label
                        anchors.right: parent.right
                        anchors.bottom: parent.bottom

                        text: Qt.formatDateTime(new Date(), "dddd, dd MMMM yyyy HH:mm AP")

                        horizontalAlignment: Text.AlignRight

                        color: "#0b678c"
                        font.bold: true
                        font.pixelSize: 12
                    }

                    PlasmaComponents.ContextMenu {
                        id: sessionMenu
                        
                        property int index: sessionModel.lastIndex
                        visualParent: sessionMenuOption   
                    }

                    Repeater {
                        parent: sessionMenu
                        model: sessionModel
                        delegate : PlasmaComponents.MenuItem {
                            text: model.name
                            checkable: true
                            checked: index === sessionMenu.index
                            onClicked : {
                                sessionMenu.index = index
                            }

                            Component.onCompleted: {
                                parent = sessionMenu
                            }
                        }
                    }
                }
            }
        }
    }

    Rectangle {
        id: actionBar
        anchors.top: parent.top;
        anchors.horizontalCenter: parent.horizontalCenter
        width: parent.width; height: 40

        Row {
            anchors.left: parent.left
            anchors.margins: 5
            height: parent.height
            spacing: 5

            Text {
                height: parent.height
                anchors.verticalCenter: parent.verticalCenter

                text: textConstants.layout
                font.pixelSize: 14
                verticalAlignment: Text.AlignVCenter
            }

            LayoutBox {
                id: layoutBox
                width: 90
                anchors.verticalCenter: parent.verticalCenter
                font.pixelSize: 14

                arrowIcon: "angle-down.png"

                KeyNavigation.backtab: session; KeyNavigation.tab: user_entry
            }
        }
    }

    Component.onCompleted: {
        if (user_entry.text === "")
            user_entry.focus = true
        else
            pw_entry.focus = true
    }
}
